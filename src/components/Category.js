import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import { Card } from 'react-bootstrap';
import {connect} from 'react-redux';
import {getCategory} from './../actions/categoryAction'
import { ToastContainer, toast } from 'react-toastify';

class Category extends Component {
    constructor(props){
        super(props)
        this.state = {
            category : []
        }
    }
    toast = (msg,type) => toast(msg, { autoClose: 2000,type : type,position : toast.POSITION.BOTTOM_RIGHT });
    componentDidMount(){
        this.props.getCategory()
    }
    render() {
        if(this.props.msg.msg_get){
            this.toast(this.props.msg.msg_get,toast.TYPE.SUCCESS)
            this.props.msg.msg_get = null
        }
        return (
            <div className="row">
                <ToastContainer autoClose={1000} />
                {
                this.props.category.map((data) => 
                    <div className="col-md-3">
                            <Card className="mt-1 mr-2" bg="info" text="white" style={{ width: '18rem' }}>
                                <Card.Header>{data.ID}</Card.Header>
                                <Card.Body>
                                <Card.Title><center><h1 className="text-dark">{data.NAME}</h1></center></Card.Title>
                                </Card.Body>
                            </Card>
                    </div>
                )}
            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        category : state.categoryR.data,
        msg : state.categoryR
    }
}
export default connect(mapStateToProps,{getCategory})(Category)