import React, { Component } from 'react'
import {Button,Form} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import {connect} from 'react-redux';
import {addArtcile,updateArticle} from './../actions/articleAction'
import { ToastContainer, toast } from 'react-toastify';

class Article extends Component {
    constructor(props){
        super(props)
        this.state = {
            ID : '',
            TITLE : '',
            DESCRIPTION : ''
        }
    }
    toast = (msg,type) => toast(msg, { autoClose: 2000,type : type,position : toast.POSITION.BOTTOM_RIGHT });
    componentDidMount(){
        if(this.props.data.article){
            let {ID,TITLE,DESCRIPTION} = this.props.data.article
            this.setState({
                ID : ID,
                TITLE : TITLE,
                DESCRIPTION : DESCRIPTION
            })
        }
    }
    onSubmitArticle  = (e) => {
        e.preventDefault()
        if(this.props.data.isAdd){
            this.props.addArtcile(this.state)
        }else{
            this.props.updateArticle(this.state)
        }
    }
    onChange = (e) => {
        this.setState({
            [e.target.name] : e.target.value
        })
    }
    render() {
        if(this.props.msg.msg_add!=null){
            this.props.onSubmitArticle()
        }
        if(this.props.msg.msg_update!=null) {
            this.props.onSubmitArticle()
        }
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-4">
                        <Form  onSubmit = {this.onSubmitArticle}>
                            <Form.Group controlId="formBasicEmail">
                                <Form.Label>Title</Form.Label>
                                <Form.Control type="text" name = "TITLE" onChange = {(e)=>this.onChange(e)} value = {this.state.TITLE} placeholder="Enter title"/>
                            </Form.Group>
                            <Form.Group controlId="formBasicEmail">
                                <Form.Label>Description</Form.Label>
                                <Form.Control type="text" placeholder="Enter description" name = "DESCRIPTION" onChange = {(e)=>this.onChange(e)} value = {this.state.DESCRIPTION}/>
                            </Form.Group> 
                            <Button variant="primary" type="submit">
                                បញ្ចូល
                            </Button>
                        </Form>
                    </div>
                </div>
                
            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        articles : state.artcicleR.data,
        msg : state.artcicleR
    }
}
export default connect(mapStateToProps,{addArtcile,updateArticle})(Article)