import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {getArticleById} from './../actions/articleAction'
import {connect} from 'react-redux';
class ViewArticle extends Component {
    load = true
    toast = (msg,type) => toast(msg, { autoClose: 2000,type : type,position : toast.POSITION.BOTTOM_RIGHT });
    componentDidMount(){
        this.props.getArticleById(this.props.match.params.id)
    }
  
    render(){
        if(this.props.msg.msg_get_by_id!=null) {
            console.log(this.props.msg.msg_get_by_id)
            this.toast(this.props.msg.msg_get_by_id,toast.TYPE.DEFAULT)
            this.props.msg.msg_get_by_id = null
            this.load = false
        }
        return  this.load 
                ?  <div><center>
                            <img src="http://www.gcorr.org/wp-content/plugins/wppdf/images/loading.gif" width="100px"/>
                        </center></div>
                :  (<div>
                      <ToastContainer autoClose={1000} />
                      <h1>Title : {this.props.article.TITLE}</h1>
                      <h3>Description : {this.props.article.DESCRIPTION}</h3>
                    </div>);    
    }
}
const mapStateToProps = (state) => {
    return {
        article : state.artcicleR.data,
        msg : state.artcicleR
    }
}
export default connect(mapStateToProps,{getArticleById})(ViewArticle)