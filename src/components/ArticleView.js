import React, { Component} from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import {connect} from 'react-redux';
import {getAlotArticle} from './../actions/articleAction'
import { ToastContainer, toast } from 'react-toastify';
import { Card } from 'react-bootstrap';
import Img from 'react-image'

class ArticleView extends Component {
    
    componentDidMount(){
        this.props.getAlotArticle()
    }
    toast = (msg,type) => toast(msg, { autoClose: 2000,type : type,position : toast.POSITION.BOTTOM_RIGHT });
    render() {
        if(this.props.msg.msg_get){
            this.toast(this.props.msg.msg_get,toast.TYPE.SUCCESS)
            this.props.msg.msg_get = null
        }
        return (
            <div className="container">
                <ToastContainer autoClose={1000} />
                <div className="row">
                    <ToastContainer autoClose={1000} />
                    {
                    Array.isArray(this.props.articles) &&   
                    this.props.articles.map((data) => { 
                        let url  = data.IMAGE ? data.IMAGE.slice(5) : '//cdn.sabay.com/cdn/media.sabay.com/media/sabay-news/Sport-News/International-Sports/soccer/Soccer54/Soccer144/Soccer147/5db2480b0ce9b_1571964900_small.jpg'
                        return <div className="col-md-3">
                                <Card className="mt-1 mr-2" bg="info" text="white" style={{ width: '18rem' }}>
                                    <Card.Header style={{
                                        height : '90px'
                                    }}>{data.TITLE}</Card.Header>
                                    <Card.Body>
                                    <Card.Title><center><img src={"//"+url} onerror="src=cdn.sabay.com/cdn/media.sabay.com/media/sabay-news/Sport-News/International-Sports/soccer/Soccer54/Soccer144/Soccer147/5db2480b0ce9b_1571964900_small.jpg" width="100px" height="100px" /></center></Card.Title>
                                    </Card.Body>
                                </Card>
                        </div>
                        }   
                    )}
                    </div>
            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        articles : state.artcicleR.data,
        msg : state.artcicleR
    }
}
export default connect(mapStateToProps,{getAlotArticle})(ArticleView)