import React, { Component } from 'react'
import {Header} from './Header'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useHistory,
    useLocation,
    Redirect
  } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import {Button} from 'react-bootstrap';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Admin from "./Admin"
import Article from './Article';
import ViewArticle from './ViewArticle';
import About from './About';
import Category from './Category'
import ArticleView from "./ArticleView";
export default class Home extends Component{
    constructor(props){
      super(props);
      this.state = {
        isAdd : false,
        isEdit : false,
        article : {}
      }
    }
    onAdd = () => {
      this.setState({
        isAdd : true,
        isEdit : false,
        article : {}
      })
    }

    onEdit = (data) => {
      this.setState({
        isAdd : false,
        isEdit : true,
        article : data
      })
    }
    onSubmitArticle = () => {
      this.setState({
               isAdd : false,
               isEdit : false, 
               article : {}
      })
    }
    
    NotFound = () =>{
      return (
        <div className="m-auto">
            <h1><h1>OPP</h1></h1>
            <Link to={"/"}>
                <Button className="btn btn-info">Go to Home page</Button>
            </Link>
            <h1>Page Not Found</h1>
        </div>
      )
    }
    Home = () => {
      return (
        <div className="m-auto">
            <h1><h1>សូមស្វា​គម​ន៍ ក្នុងការចូលមកគ្រប់គ្រងអត្ថបទ</h1></h1>
            <Link to={"/admin"}>
                <Button className="btn btn-info">Manage</Button>
            </Link>
            
        </div>
      )
    }
    toast = (msg,type) => toast(msg, { autoClose: 2000,type : type,position : toast.POSITION.BOTTOM_RIGHT });
    render(){
        return (
            <div className="vh-100" style={{
              backgroundImage : "url('https://thumbs-prod.si-cdn.com/PT-enupw3YGzvb_SHpFOxkWUI_M=/800x600/filters:no_upscale()/https://public-media.si-cdn.com/filer/70/e0/70e0989e-646e-4537-ae8e-7bbf863db2fd/ebjj1g.jpg')",
              backgroundRepeat : 'no-repeat',
              backgroundSize : 'cover'
            }}>
                <Header/>
                <ToastContainer autoClose={1000} />
                <Router>
                    <Switch>
                        <AdminRoute path={["/admin"]}>
                            {
                                !(this.state.isAdd||this.state.isEdit) ? <Admin onAdd = {this.onAdd} onEdit = {this.onEdit}/> : <Article data = {this.state} onSubmitArticle = {this.onSubmitArticle}/>
                            }
                        </AdminRoute>
                        <Route exact path={["/","/home"]} render={()=><this.Home/>} />
                        <Route path="/login" component={()=> <LoginPage/> } />
                        <Route path="/category" component={()=> <Category/> } />
                        <Route path="/about" component={()=> <About/> } />
                        <Route path="/article" component={()=> <ArticleView/> } />
                        <Route path="/detail/:id" component={ ViewArticle } />
                        <Route path="*" component={()=><this.NotFound></this.NotFound>}/>
                       
                    </Switch>
                </Router>
            </div>
    )
    }
}

const fakeAuth = {
    isAuthenticated: false,
    authenticate(cb) {
      fakeAuth.isAuthenticated = true;
      setTimeout(cb, 100); // fake async
    },
    signout(cb) {
      fakeAuth.isAuthenticated = false;
      setTimeout(cb, 100);
    }
};

function LoginPage(){
  let history = useHistory();
  let location = useLocation();

  let { from } = location.state || { from: { pathname: "/" } };
  let login = () => {
    
    fakeAuth.authenticate(() => {
      history.replace({
        pathname : from.pathname
      });
    });
  };
  return (
    <div>
      <h1>សូមធ្វើការ Login ជាមុនសិនដើម្បីចូលទៅកាន់ទំព័រ {from.pathname}</h1>
      <Button onClick={login}>Log in</Button>
    </div>
  );
}

function AdminRoute({ children, ...rest }){
    return (
        <Route
          {...rest}
          render={({ location }) =>
            fakeAuth.isAuthenticated ? (
              children
            ) : (
              <Redirect
                to={{
                  pathname: "/login",
                  state: { from: location }
                }}
              />
            )
          }
        />
    );
}

