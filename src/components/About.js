import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import './../App.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'


export default class About extends Component {
render() {
return (
<div>
    <div id="index">
        <div class="container main">
            <div class="row home">
                <div id="index_left" class="col-md-6 left">
                    <img class="img-responsive img-rabbit img-rounded" src="http://rabbitboy.me/images/raden.png" />
                </div>
                <div id="index_right" class="col-md-6 text-center right">
                    <div class="logo">
                        <img src="http://rabbitboy.me/images/rabbit.png" />
                        <h4>I am rabbit</h4>
                    </div>
                    <p class="home-description">
                        Hi, I am Rabbit, Web developer and Mobile developer from Korea Software HRD, Cambodia. I am
                        passionate about backend development and devops. I really love to code.
                    </p>
                    <div class="social">
                        <a href="https://www.instagram.com/raden_hor/"><i class="fa fa-instagram"
                                aria-hidden="true"></i></a>
                        <a href="https://twitter.com/HorRaden"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <a href="https://www.facebook.com/radenhor99"><i class="fa fa-facebook"
                                aria-hidden="true"></i></a>
                                
                    </div>
                </div>
            </div>
            <div class="text-center">
                <h1>No place like 127.0.0.1</h1>
            </div>

        </div>
    </div>
</div>
)
}
}