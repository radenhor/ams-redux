import React, { Component } from 'react'
import {Table,Button} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Link} from "react-router-dom";
import {connect} from 'react-redux';
import SweetAlert from 'react-bootstrap-sweetalert';
import {getArticle,deleteArticle,getArticleById} from './../actions/articleAction'
import { ToastContainer, toast } from 'react-toastify';
class Admin extends Component {
    constructor(props){
        super(props)
        this.state = {
            alert : '',
            articles : []
        }
    }
    toast = (msg,type) => toast(msg, { autoClose: 2000,type : type,position : toast.POSITION.BOTTOM_RIGHT });
    isDelete = false
    componentDidMount(){
        if(this.props.msg.msg_add!=null){
            this.toast(this.props.msg.msg_add,toast.TYPE.INFO)
            this.props.msg.msg_add = null
        }
        else if(this.props.msg.msg_update!=null) {
            this.toast(this.props.msg.msg_update,toast.TYPE.INFO)
            this.props.msg.msg_update = null
        }
        this.props.getArticle()
        this.isDelete = false
    }
    componentDidUpdate(){
        // this.props.msg.msg_get=null
    }
    onDelete = (id) => {
        const getAlert = () => (
          <SweetAlert 
            danger 
            showCancel
            confirmBtnText="Yes, delete it!"
            confirmBtnBsStyle="danger"
            cancelBtnBsStyle="default"
            title="Are you sure?"
            onCancel = {()=> this.setState({alert: null})}
            onConfirm={() => {
                this.props.deleteArticle(id)
                this.setState({alert:null})
            }}
          >
          </SweetAlert>
        );
        this.isDelete = true
        this.setState({
          alert: getAlert()
        });
    }
    onEdit = (id) => {
        this.props.getArticleById(id)
    }

    render() {
        if(this.props.msg.msg_delete!=null) {
            this.toast(this.props.msg.msg_delete,toast.TYPE.ERROR)
            this.props.msg.msg_delete = null
            this.props.getArticle()
        }
        if(this.props.msg.msg_get_by_id!=null) {
            this.props.onEdit(this.props.articles)
        }
        if(this.props.msg.msg_get!=null&&!this.isDelete){
            // this.setState({
            //     ...this.state,
            //     articles : this.props.articles
            // })
            this.toast(this.props.msg.msg_get,toast.TYPE.SUCCESS)
            // this.props.msg.msg_get = null
        }
        return (
            <div>
                {/* {this.props.msg.msg_get==null ? <div>Fetching</div> :  */}
                    <div>
                        <ToastContainer autoClose={1000} />
                        {this.state.alert}
                        <div className="m-3">
                            <h1>ការរៀនរីអែក</h1>
                            <Button className="btn btn-primary" onClick = { ()=> this.props.onAdd()}>Add</Button>
                            <h1>បញ្ជីអត្ថបទ</h1>
                        </div>
                        <Table striped bordered hover variant="dark">
                    <thead>
                        <tr>
                        <th>#ID</th>
                        <th>TITLE</th>
                        <th>DESCRIPTION</th>
                        <th>CREATED_DATE</th>   
                        <th>ACTION</th>
                        </tr>
                    </thead>
                    <tbody>
                    {
                       Array.isArray(this.props.articles) ?
                       this.props.articles.map((data)=>
                            <tr key={data.ID}>
                                <td>{data.ID}</td>
                                <td>{data.TITLE}</td>
                                <td>{data.DESCRIPTION}</td>
                                <td>{data.CREATED_DATE}</td>
                                <td>
                                    <Link to={"/detail/"+data.ID}>
                                            <Button className="btn btn-info">View</Button>
                                    </Link>
                                    <Button onClick={()=> this.onEdit(data.ID)} className="btn btn-primary mx-2">Edit</Button>
                                    <Button className="btn btn-danger" onClick = {()=>this.onDelete(data.ID)}>Delete</Button>
                                </td>
                            </tr>
                        )
                        :
                        ''
                    }   
                    </tbody>
                </Table>
                    </div>
                {/* } */}
            </div>
          )
    }

}
const mapStateToProps = (state) => {
    return {
        articles : state.artcicleR.data,
        msg : state.artcicleR
    }
}
export default connect(mapStateToProps,{getArticle,deleteArticle,getArticleById})(Admin)