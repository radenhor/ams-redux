import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import { Navbar, Nav,Form,FormControl,Button } from 'react-bootstrap';


export  class Header extends Component {
render() {
return (
<div>
    <Navbar bg="light" expand="lg">
        <Navbar.Brand href="/home">ទំព័រដើម</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
                <Nav.Link href="/article">អត្ថបទ</Nav.Link>
                <Nav.Link href="/category">ប្រភេទ</Nav.Link>
                <Nav.Link href="/about">អំពីខ្ញុំ</Nav.Link>
            </Nav>
            <Form inline>
                <Button href="/admin" variant="outline-success">ចូលប្រព័ន្ធ</Button>
            </Form>
        </Navbar.Collapse>
    </Navbar>
</div>
)
}
}