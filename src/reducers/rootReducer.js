import {combineReducers} from 'redux'
import {artcicleReducer} from './articleReducer'
import {categoryReducer} from './categoryReducer'

export const rootReducer = combineReducers({
    artcicleR : artcicleReducer,
    categoryR : categoryReducer
})