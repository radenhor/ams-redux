import {actionType} from './../actions/actionType';

const initState = {
    data : [],
    msg_get : null
}

export const categoryReducer = (state = initState,action) => {
    switch (action.type) {
        case actionType.GET_CATEGORY :
            return {
                data : action.data.DATA,
                msg_get : action.msg
            }
        default :
            return state;
    }
}