import {actionType} from './../actions/actionType';

const initState = {
    data : [],
    msg_delete : null,
    msg_add : null,
    msg_update : null,
    msg_get : null,
    msg_get_by_id : null
}

export const artcicleReducer = (state = initState,action) => {
    switch(action.type){
        case actionType.GET_ARTCILE : 
            return {
                ...state,
                data : action.data.DATA,
                msg_add : null,
                msg_update : null,
                msg_get : action.msg,
                msg_delete : null,
                msg_get_by_id : null
            } 
        case actionType.GET_ARTCILE_BY_ID : 
            return {
                ...state,
                data : action.data.DATA,
                msg_add : null,
                msg_update : null,
                msg_get : null,
                msg_delete : null,
                msg_get_by_id : action.msg
            }  
        case actionType.PUT_ARTICLE : 
            return {
                ...state,
                data : action.data.DATA,
                msg_add : null,
                msg_update : action.msg,
                msg_get : null,
                msg_delete : null,
                msg_get_by_id : null
            }          
        case actionType.DELETE_ARTICLE :
            let newData = state.data.filter(data => data.ID!==action.data.DATA.ID)
            return {
                data : newData,
                msg_delete : action.msg,
                msg_add : null,
                msg_update : null,
                msg_get : null,
                msg_get_by_id : null
            }
        case actionType.ERROR_DELETE :
            return {
                ...state,
                msg_delete : action.msg,
                msg_add : null,
                msg_update : null,
                msg_get : null,
                msg_get_by_id : null
            }
        case actionType.POST_ARTICLE : 
            return {
                data : action.data.DATA,
                msg_delete : null,
                msg_add : action.msg,
                msg_update : null,
                msg_get : null,
                msg_get_by_id : null
            }
        default : 
            return state    
    }
}