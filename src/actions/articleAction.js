import {actionType} from './actionType';
import axios from 'axios';
 
export const getArticle = () => {
    return dispatch => {
        axios.get("http://api-ams.me/v1/api/articles?page=1&limit=15")
        .then(res =>  dispatch({type:actionType.GET_ARTCILE,data:res.data,msg : res.data.MESSAGE}))
        .catch(res => dispatch({type:actionType.ERROR,data:res.data}))
    }
}

export const getAlotArticle = () => {
    return dispatch => {
        axios.get("http://api-ams.me/v1/api/articles?page=1&limit=100")
        .then(res =>  dispatch({type:actionType.GET_ARTCILE,data:res.data,msg : res.data.MESSAGE}))
        .catch(res => dispatch({type:actionType.ERROR,data:res.data}))
    }
}

export const deleteArticle = (id) => {
    return dispatch => {
        axios({
            method : "DELETE",
            url : "http://api-ams.me/v1/api/articles/"+id,
        })
        .then(res => dispatch({type : actionType.DELETE_ARTICLE,data:res.data,msg : res.data.MESSAGE}))
        .catch(res => dispatch({type : actionType.ERROR_DELETE,msg: "Error delete!!!"}))
    }
}

export const addArtcile = (data) => {
    return dispatch => {
        axios({
            method : "POST",
            url : "http://api-ams.me/v1/api/articles",
            data : data
        })
        .then(res => dispatch({type : actionType.POST_ARTICLE,data:res.data,msg : res.data.MESSAGE}))
        .catch(res => dispatch({type : actionType.ERROR_ADD,msg: "Error add!!!"}))
    }
}

export const getArticleById = (id) => {
    return dispatch => {
        axios.get("http://api-ams.me/v1/api/articles/"+id)
        .then(res =>  dispatch({type:actionType.GET_ARTCILE_BY_ID,data:res.data,msg : res.data.MESSAGE}))
        .catch(res => dispatch({type:actionType.ERROR,data:res.data}))
    }
    
}

export const updateArticle = (data) =>{
    return dispatch => {
        axios({
            method : "PUT",
            url : "http://api-ams.me/v1/api/articles/"+data.ID,
            data : {
                "TITLE" : data.TITLE,
                "DESCRIPTION" : data.DESCRIPTION
            }
        })
        .then(res => {dispatch({type : actionType.PUT_ARTICLE,data:res.data,msg : res.data.MESSAGE})})
        .catch(res => dispatch({type : actionType.ERROR_UPDATE,msg: "Error update!!!"}))
    }
}