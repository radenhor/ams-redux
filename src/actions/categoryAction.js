import {actionType} from './actionType';
import axios from 'axios';

export const getCategory = () => {
    return dispatch => {
        axios.get("http://api-ams.me/v1/api/categories")
        .then(res => { dispatch({type:actionType.GET_CATEGORY,data:res.data,msg : res.data.MESSAGE})  })
        .catch(res => dispatch({type:actionType.ERROR,data:res.data}))
    }
}
