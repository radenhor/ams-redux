export const actionType = {
    GET_ARTCILE : "GET_ARTICLE",
    POST_ARTICLE : "POST_ARTICLE",
    PUT_ARTICLE : "PUT_ARTICLE",
    DELETE_ARTICLE : "DELETE_ARTICLE",
    ERROR_DELETE : "ERROR_DELETE",
    GET_ARTCILE_BY_ID : "GET_ARTCILE_BY_ID",
    GET_CATEGORY : "GET_CATEGORY",
    ERROR_ADD : "ERROR_ADD",
    ERROR_UPDATE : "ERROR_UPDATE"
}